/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package shapesimulation;

/**
 *
 * @author DHAIRYA
 */
public class square {
    private String Area;
    private String perimeter;

    public square(String Area, String perimeter) {
        this.Area = Area;
        this.perimeter = perimeter;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String Area) {
        this.Area = Area;
    }

    public String getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(String perimeter) {
        this.perimeter = perimeter;
    }
    
}
