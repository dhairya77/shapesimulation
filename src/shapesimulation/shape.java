/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package shapesimulation;

/**
 *
 * @author DHAIRYA
 */
public class shape {
    
    private String area;
    private String perimeter;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(String perimeter) {
        this.perimeter = perimeter;
    }

    public shape(String area, String perimeter) {
        this.area = area;
        this.perimeter = perimeter;
    }
    
}
